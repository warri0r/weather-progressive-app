var cacheName = 'weatherPWA-set-6-1',
    dataCacheName = 'weatherData-v1',
    filesToCache = [
        '/',
        '/index.html',
        '/scripts/app.js',
        '/styles/inline.css',
        '/images/clear.png',
        '/images/cloudy-scattered-showers.png',
        '/images/cloudy.png',
        '/images/fog.png',
        '/images/ic_add_white_24px.svg',
        '/images/ic_refresh_white_24px.svg',
        '/images/partly-cloudy.png',
        '/images/rain.png',
        '/images/scattered-showers.png',
        '/images/sleet.png',
        '/images/snow.png',
        '/images/thunderstorm.png',
        '/images/wind.png'        
    ];

self.addEventListener('install', function(event) {
    console.log('[ServiceWorker] install event called');

    var cachingFunction = caches.open(cacheName).then(function(cache) {
        console.log('[ServiceWorker] caching app shell');

        return cache.addAll(filesToCache);
    });
    
    event.waitUntil(cachingFunction);

});

self.addEventListener('activate', function(e) {
    console.log('[ServiceWorker] activate event called');

    e.waitUntil(caches.keys().then(function(keyList) {
            return Promise.all(keyList.map(function(key) {
                if(key !== cacheName && key !== dataCacheName) {
                    console.log('[ServiceWorker] removing old cache');
                    return caches.delete(key);
                }
            }));
        }));    

    return self.clients.claim();
});

self.addEventListener('fetch', function(event) {
    console.log('[ServiceWorker] fetch event called for ', event.request.url);

    var dataUrl = 'https://query.yahooapis.com/v1/public/yql';
    if(event.request.url.indexOf(dataUrl) > -1) {
    /*
     * When the request URL contains dataUrl, the app is asking for fresh
     * weather data. In this case, the service worker always goes to the
     * network and then caches the response. This is called the "Cache then
     * network" strategy:
     * https://jakearchibald.com/2014/offline-cookbook/#cache-then-network
     */        
        event.respondWith(caches.open(dataCacheName).then(function(cache) {
            return fetch(event.request).then(function(response) {
                cache.put(event.request.url, response.clone());

                return response;
            });
        }));
    } else {
    /*
     * The app is asking for app shell files. In this scenario the app uses the
     * "Cache, falling back to the network" offline strategy:
     * https://jakearchibald.com/2014/offline-cookbook/#cache-falling-back-to-network
     */        
        event.respondWith(
            caches.match(event.request).then(function(response) {
                return response || fetch(event.request);
            }));
    }
});